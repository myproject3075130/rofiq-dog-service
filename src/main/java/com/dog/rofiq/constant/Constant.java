package com.dog.rofiq.constant;

public class Constant {

    public static final String BREED_SHEEPDOG = "sheepdog";
    public static final String BREED_SHIBA = "shiba";
    public static final String BREED_TERRIER = "terrier";
    public static final String CACHE_BREED = "BREEDS";

}
