package com.dog.rofiq.repository;

import com.dog.rofiq.model.Breed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BreedRepository extends JpaRepository<Breed, Integer> {
    Optional<Breed> findByName(String name);

    Page<Breed> findByName(String name, Pageable pageable);

    @Query(value = "select * from breed where name like %:name%", nativeQuery = true)
    List<Breed> findBreedByName(String name);

}
