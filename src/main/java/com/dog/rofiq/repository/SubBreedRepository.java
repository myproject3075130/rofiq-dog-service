package com.dog.rofiq.repository;

import com.dog.rofiq.model.Breed;
import com.dog.rofiq.model.SubBreed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubBreedRepository extends JpaRepository<SubBreed, Integer> {
    List<SubBreed> findByBreed(Breed breed);
}
