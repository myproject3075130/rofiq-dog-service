package com.dog.rofiq.repository;

import com.dog.rofiq.model.Images;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Images, Integer> {
    List<Images> findByReferenceId(Integer referenceId);
}
