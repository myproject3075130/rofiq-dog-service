package com.dog.rofiq.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Breed implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "breed", cascade = {CascadeType.ALL})
    private List<SubBreed> subBreeds;


}
