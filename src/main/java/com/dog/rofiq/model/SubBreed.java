package com.dog.rofiq.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class SubBreed implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    @JsonBackReference
    @ManyToOne(cascade= {CascadeType.MERGE})
    @JoinColumn(name = "breed_id")
    private Breed breed;
    private String name;
}
