package com.dog.rofiq.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Images implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    private String imageUrl;
    private Integer referenceId;
    private Boolean hasSubBreed;
}
