package com.dog.rofiq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RofiqDogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RofiqDogServiceApplication.class, args);
	}

}
