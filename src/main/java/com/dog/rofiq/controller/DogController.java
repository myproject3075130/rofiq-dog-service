package com.dog.rofiq.controller;

import com.dog.rofiq.dto.request.BreedRequest;
import com.dog.rofiq.dto.response.BaseResponse;
import com.dog.rofiq.service.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin("*")
@RestController
@RequestMapping("/breed")
public class DogController {

    @Autowired
    public DogService dogService;

    @GetMapping("/detail/{id}")
    public BaseResponse detailBreed(@PathVariable("id") Integer id) {
        return dogService.detailBreed(id);
    }

    @PostMapping("/save")
    public BaseResponse save(@RequestBody @Valid BreedRequest request) {
        return dogService.saveBreed(request);
    }

    @DeleteMapping("/delete/{id}")
    public BaseResponse delete(@PathVariable("id") Integer id) {
        return dogService.delete(id);
    }

    @PostMapping("/find")
    public BaseResponse findBreed(@RequestParam String name) {
        return dogService.findBreedByName(name);
    }

}
