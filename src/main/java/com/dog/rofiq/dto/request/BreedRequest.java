package com.dog.rofiq.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BreedRequest {
    private Integer id;
    @NotNull(message = "breed must be not null")
    @NotEmpty(message = "breed must be not empty")
    @NotBlank(message = "breed must be not blank")
    private String breed;
    private SubBreedRequest subBreed;
    private List<ImageUrlRequest> imageUrlRequests;
}
