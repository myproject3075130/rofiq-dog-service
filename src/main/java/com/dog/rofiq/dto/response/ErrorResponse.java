package com.dog.rofiq.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel
@Builder
@Data
@NoArgsConstructor
public class ErrorResponse {
    private String message;
    private boolean success;
    private int errorCode;
}
