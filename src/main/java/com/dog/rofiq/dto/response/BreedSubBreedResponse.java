package com.dog.rofiq.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BreedSubBreedResponse {
    private Integer id;
    private String breed;
    private List<SubBreedResponse> subBreeds;

}
