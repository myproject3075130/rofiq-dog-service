package com.dog.rofiq.service;

import com.dog.rofiq.constant.Constant;
import com.dog.rofiq.dto.request.BreedRequest;
import com.dog.rofiq.dto.response.*;
import com.dog.rofiq.exception.DataNotFoundException;
import com.dog.rofiq.model.Breed;
import com.dog.rofiq.model.Images;
import com.dog.rofiq.model.SubBreed;
import com.dog.rofiq.repository.BreedRepository;
import com.dog.rofiq.repository.ImageRepository;
import com.dog.rofiq.repository.SubBreedRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class DogService {

    @Autowired
    private BreedRepository breedRepository;
    @Autowired
    private SubBreedRepository subBreedRepository;
    @Autowired
    private ImageRepository imageRepository;

    public BaseResponse detailBreed(Integer id) {
        var breedOpt = breedRepository.findById(id);
        Breed breed = null;
        SubBreedResponse subBreedResponse;
        Integer refId;
        if (breedOpt.isEmpty()) {
            SubBreed subBreed = subBreedRepository.findById(id).orElseThrow(
                    () -> new DataNotFoundException("subbreed not found")
            );
            breed = subBreed.getBreed();
            subBreedResponse = SubBreedResponse.builder()
                    .id(subBreed.getId())
                    .name(subBreed.getName())
                    .build();
            refId = subBreed.getId();
        } else {
            breed = breedOpt.get();
            subBreedResponse = SubBreedResponse.builder()
                    .id(breed.getId())
                    .name(breed.getName())
                    .build();
            refId = breed.getId();
        }
        List<Images> images = imageRepository.findByReferenceId(refId);
        return BaseResponse.builder()
                .message("success")
                .result(
                        BreedDetailResponse.builder()
                                .breed(breed.getName())
                                .subBreedResponse(subBreedResponse)
                                .imagesResponses(images.stream().map(i -> ImagesResponse.builder()
                                        .id(i.getId())
                                        .imageUrl(i.getImageUrl())
                                        .build()).toList())
                                .build()
                )
                .build();
    }

    @org.springframework.cache.annotation.CachePut(key = "#request.id", value = "SAVEBREED")
    public BaseResponse saveBreed(BreedRequest request) {
        Integer refId;
        Breed breed = breedRepository.save(Breed.builder()
                .id(request.getId())
                .name(request.getBreed())
                .build());
        if (StringUtils.hasText(request.getSubBreed().getName())) {
            SubBreed subBreed = subBreedRepository.save(SubBreed.builder()
                    .id(request.getSubBreed().getId())
                    .breed(breed)
                    .name(request.getSubBreed().getName())
                    .build());
            refId = subBreed.getId();
            log.info("save with sub breed, refId : {}", refId);
        } else {
            refId = breed.getId();
            log.info("refId : {}", refId);
        }
        request.getImageUrlRequests().forEach(i -> imageRepository.save(Images.builder()
                .id(i.getId())
                .hasSubBreed(Objects.nonNull(request.getSubBreed()))
                .imageUrl(i.getImageUrl())
                .referenceId(refId)
                .build()));
        return BaseResponse.builder().message("success").build();
    }

    public BaseResponse delete(Integer id) {
        var breedOpt = breedRepository.findById(id);
        Integer refId;
        if (breedOpt.isPresent()) {
            Breed breed = breedOpt.get();
            refId = breed.getId();
            breedRepository.delete(breed);
        } else {
            SubBreed subBreed = subBreedRepository.findById(id).orElseThrow(
                    () -> new DataNotFoundException("sub breed data not found")
            );
            Breed breed = subBreed.getBreed();
            refId = breed.getId();
            breedRepository.delete(breed);
            subBreedRepository.delete(subBreed);
        }
        List<Images> images = imageRepository.findByReferenceId(refId);
        imageRepository.deleteAll(images);
        return BaseResponse.builder().message("success").build();
    }

    public BaseResponse findBreedByName(String prm) {
        List<Breed> breedList = findByName(prm);
        return BaseResponse.builder()
                .message("success")
                .result(breedList.stream().map(this::parseToBreedResponse))
                .build();
    }

    @Cacheable(key = "#prm", value = Constant.CACHE_BREED)
    private List<Breed> findByName(String name) {
        return breedRepository.findBreedByName(name);
    }

    private List<BreedResponse> parseToBreedResponse(Breed breed) {
        List<BreedResponse> resultList = new ArrayList<>();
        if (!breed.getSubBreeds().isEmpty()) {
            List<SubBreed> subBreeds = subBreedRepository.findByBreed(breed);
            subBreeds.forEach(s -> {
                List<Images> images = imageRepository.findByReferenceId(s.getId());
                resultList.add(
                        BreedResponse.builder()
                                .id(s.getId())
                                .name(s.getName())
                                .imagesResponses(images.stream().map(i ->
                                        ImagesResponse.builder()
                                                .id(i.getId())
                                                .imageUrl(i.getImageUrl())
                                                .build()).toList())
                                .build()
                );
            });
        } else {
            List<Images> images = imageRepository.findByReferenceId(breed.getId());
            resultList.add(
                    BreedResponse.builder()
                            .id(breed.getId())
                            .name(breed.getName())
                            .imagesResponses(images.stream().map(i ->
                                    ImagesResponse.builder()
                                            .id(i.getId())
                                            .imageUrl(i.getImageUrl())
                                            .build()).toList())
                            .build()
            );
        }
        return resultList;
    }

}
