package com.dog.rofiq.service;

import com.dog.rofiq.dto.response.DogImagesResponse;
import com.dog.rofiq.model.Breed;
import com.dog.rofiq.model.Images;
import com.dog.rofiq.model.SubBreed;
import com.dog.rofiq.repository.BreedRepository;
import com.dog.rofiq.repository.ImageRepository;
import com.dog.rofiq.repository.SubBreedRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.IntStream;

@Configuration
@Slf4j
public class FetchDogRest {

    private static final String baseUrl = "https://dog.ceo/api/breed/";
    private static final String endUrl = "/images/random";

    @Autowired
    private BreedRepository breedRepository;
    @Autowired
    private SubBreedRepository subBreedRepository;
    @Autowired
    private ImageRepository imageRepository;

    RestTemplate restTemplate = new RestTemplate();
    ObjectMapper objectMapper = new ObjectMapper();

    @Bean
    @SneakyThrows
    public void fetchBreedDog() {
        fetchWithSubBreed("sheepdog");
        fetchWithSubBreed("terrier");
        fetchShibaDog("shiba");
    }

    public void fetchShibaDog(String param) {
        Breed breed = breedRepository.save(Breed.builder()
                .name(param)
                .build());
        int size = 5;
        IntStream.range(0, size).forEach(i -> {
            ResponseEntity<DogImagesResponse> response = restTemplate.getForEntity(
                    baseUrl.concat(param).concat(endUrl), DogImagesResponse.class
            );
            if (Objects.nonNull(response.getBody())) {
                DogImagesResponse dogImagesResponse = response.getBody();
                imageRepository.save(Images.builder()
                        .imageUrl(dogImagesResponse.getMessage())
                        .hasSubBreed(false)
                        .referenceId(breed.getId())
                        .build());
            }
        });
        log.info("save to image");
    }

    @SneakyThrows
    public void fetchWithSubBreed(String param) {
        Breed breed = breedRepository.save(Breed.builder()
                .name(param)
                .build());
        ResponseEntity<String> responseAllData = restTemplate.getForEntity("https://dog.ceo/api/breeds/list/all", String.class);
        HashMap allDataMessageResponse = objectMapper.readValue(responseAllData.getBody(), HashMap.class);
        HashMap allDataMessageResponses = (HashMap) allDataMessageResponse.get("message");
        List<String> subSheppyDog = (List<String>) allDataMessageResponses.get(param);
        subSheppyDog.forEach(s -> {
            SubBreed subBreed = subBreedRepository.save(SubBreed.builder()
                    .breed(breed)
                    .name(param.concat("-").concat(s))
                    .build());
            int size = 4;
            IntStream.range(0, size).forEach(i -> {
                ResponseEntity<DogImagesResponse> responseImage = restTemplate.getForEntity(
                        baseUrl.concat(param.concat("/").concat(s)).concat(endUrl), DogImagesResponse.class
                );
                if (Objects.nonNull(responseImage.getBody())) {
                    DogImagesResponse dogImagesResponse = responseImage.getBody();
                    imageRepository.save(Images.builder()
                            .imageUrl(dogImagesResponse.getMessage())
                            .hasSubBreed(true)
                            .referenceId(subBreed.getId())
                            .build());
                }
            });
        });
        log.info("save to image sub breed");
    }

}
