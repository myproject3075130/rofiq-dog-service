package com.dog.rofiq.config;

import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


@EnableCaching
@Configuration
public class CacheConfiguration {
  @Value("${redis.ttl:10}")
  private Integer ttl;

  @Bean
  public CacheManager redisCacheManager(RedissonClient redissonClient) {
    long value = ttl.longValue();
    Map<String, CacheConfig> config = new HashMap<>();
    // create "testMap" spring cache with ttl = 10 minute and maxIdleTime = 10 minute
    config.put("BREEDS", new CacheConfig( value * 60 * 1_000, value * 60 * 1_000));
    config.put("SAVEBREED", new CacheConfig( value * 60 * 1_000, value * 60 * 1_000));

    RedissonSpringCacheManager redissonCacheManager = new RedissonSpringCacheManager(redissonClient, config);
    redissonCacheManager.setAllowNullValues(false);
    redissonCacheManager.setCodec(new SerializationCodec());
    return redissonCacheManager;
  }
}
