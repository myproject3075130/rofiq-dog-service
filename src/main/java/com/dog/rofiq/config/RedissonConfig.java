package com.dog.rofiq.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {
  @Value("${redis.host}")
  private String host;
  @Value("${redis.port}")
  private Integer port;
  @Value("${redis.timeout}")
  private Integer timeout;
  @Value("${redis.pool-size}")
  private Integer poolSize;
  @Value("${redis.idle-size}")
  private Integer idleSize;
  @Value("${redis.db}")
  private Integer db;

  @Bean
  public RedissonClient redissonClient() {
    Config config = new Config();
    config.useSingleServer()
        .setConnectionPoolSize(poolSize)
        .setConnectionMinimumIdleSize(idleSize)
        .setConnectTimeout(timeout)
        .setDatabase(db)
        .setAddress("redis://" + host + ":" + port);
    return Redisson.create(config);
  }
}
