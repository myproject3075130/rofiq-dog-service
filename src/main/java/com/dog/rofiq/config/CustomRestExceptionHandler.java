package com.dog.rofiq.config;

import com.dog.rofiq.dto.response.ErrorResponse;
import com.dog.rofiq.exception.DataNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.Objects;

@ControllerAdvice
@Slf4j
public class CustomRestExceptionHandler {
    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponse> handleAll(Exception ex, WebRequest request) {
        log.info("Exception class name : {}", ex.getClass().getName());
        log.info(ex.getLocalizedMessage(), ex);
        final var allowedExceptions = List.of(DataNotFoundException.class);
        if (allowedExceptions.stream().anyMatch(c -> c.isInstance(ex))) {
            return ResponseEntity.ok(ErrorResponse.builder()
                            .message(ex.getLocalizedMessage())
                            .errorCode(500)
                            .success(false)
                    .build());
        } else if (ex instanceof MethodArgumentNotValidException) {
            return ResponseEntity.ok(ErrorResponse.builder()
                    .message(Objects.requireNonNullElse(((MethodArgumentNotValidException) ex).getFieldError(),
                            new FieldError("", "", "")).getDefaultMessage())
                            .errorCode(400)
                            .success(false)
                    .build());
        }
        return ResponseEntity.ok(ErrorResponse.builder()
                .errorCode(500)
                .success(false)
                .message("Something went wrong, please try again")
                .build());
    }
}
