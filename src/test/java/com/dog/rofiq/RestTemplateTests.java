package com.dog.rofiq;

import com.dog.rofiq.dto.response.DogImagesResponse;
import com.dog.rofiq.model.Breed;
import com.dog.rofiq.model.Images;
import com.dog.rofiq.repository.BreedRepository;
import com.dog.rofiq.repository.ImageRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@MockitoSettings(strictness = Strictness.LENIENT)
@Slf4j
public class RestTemplateTests {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    BreedRepository breedRepository;
    @Mock
    ImageRepository imageRepository;
    @Mock
    ObjectMapper objectMapper = new ObjectMapper();

    private static final DogImagesResponse dogImagesResponse = DogImagesResponse.builder()
            .message("https://images.dog.ceo/breeds/affenpinscher/n02110627_4457.jpg")
            .status("success")
            .build();
    private static final Breed bred = Breed.builder()
            .id(1)
            .name("shiba")
            .build();
    private static final Images images = Images.builder()
            .id(1)
            .referenceId(1)
            .hasSubBreed(false)
            .imageUrl("https://images.dog.ceo/breeds/affenpinscher/n02110627_4457.jpg")
            .build();

    @Test
    void given_success_fetch_shiba_dog() {
        when(restTemplate.getForEntity("https://dog.ceo/api/breed/shiba/images/random", DogImagesResponse.class))
          .thenReturn(new ResponseEntity<>(dogImagesResponse, HttpStatus.OK));
        when(breedRepository.save(any())).thenReturn(bred);
        when(imageRepository.save(any())).thenReturn(images);
    }

    @Test
    @SneakyThrows
    void given_success_fetch_all_data() {
        when(restTemplate.getForEntity("https://dog.ceo/api/breeds/list/all", DogImagesResponse.class))
                .thenReturn(new ResponseEntity<>(dogImagesResponse, HttpStatus.OK));
    }


}
