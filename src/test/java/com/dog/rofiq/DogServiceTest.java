package com.dog.rofiq;

import com.dog.rofiq.constant.Constant;
import com.dog.rofiq.dto.response.BaseResponse;
import com.dog.rofiq.dto.response.BreedResponse;
import com.dog.rofiq.dto.response.ImagesResponse;
import com.dog.rofiq.model.Breed;
import com.dog.rofiq.model.Images;
import com.dog.rofiq.model.SubBreed;
import com.dog.rofiq.repository.BreedRepository;
import com.dog.rofiq.repository.ImageRepository;
import com.dog.rofiq.repository.SubBreedRepository;
import com.dog.rofiq.service.DogService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Optional;

@MockitoSettings(strictness = Strictness.LENIENT)
@Slf4j
public class DogServiceTest {
    @Mock
    SubBreedRepository subBreedRepository;
    @Mock
    BreedRepository breedRepository;
    @Mock
    ImageRepository imageRepository;
    @Mock
    DogService dogService;

    private static final Breed breed = Breed.builder()
            .id(1)
            .name("sheepdog")
            .build();

    private static final List<SubBreed> subBreeds = List.of(
            SubBreed.builder().id(1).breed(breed).name("sheepdog-english").build(),
            SubBreed.builder().id(2).breed(breed).name("sheepdog-shetland").build()
    );

    private static final List<Images> imageList = List.of(
            Images.builder().id(1).imageUrl("https://images.dog.ceo/breeds/affenpinscher/n02110627_4457.jpg").build(),
            Images.builder().id(12).imageUrl("https://images.dog.ceo/breeds/affenpinscher/n0200627_4457.jpg").build(),
            Images.builder().id(1).imageUrl("https://images.dog.ceo/breeds/affenpinscher/n03450627_4457.jpg").build()
    );

    private static final Images image = Images.builder().id(1).imageUrl("https://images.dog.ceo/breeds/affenpinscher/n02110627_4457.jpg").build();

    private static final BaseResponse baseResponse = BaseResponse.builder()
            .message("success")
            .result(
                    BreedResponse.builder()
                            .id(1)
                            .name("shiba")
                            .imagesResponses(
                                    List.of(
                                            ImagesResponse.builder().id(1)
                                                    .imageUrl("https://images.dog.ceo/breeds/affenpinscher/n02110627_4457.jpg")
                                                    .build(),
                                            ImagesResponse.builder().id(1)
                                                    .imageUrl("https://images.dog.ceo/breeds/affenpinscher/n0200627_4457.jpg")
                                                    .build(),
                                            ImagesResponse.builder().id(1)
                                                    .imageUrl("https://images.dog.ceo/breeds/affenpinscher//n03450627_4457.jpg")
                                                    .build()
                                    )
                            )
                            .build()
            )
            .build();

    private static final BaseResponse responseSuccessSave = BaseResponse.builder().message("success").build();

    private static BaseResponse responseEmpty = BaseResponse.builder()
            .message("success")
            .result(
                    BreedResponse.builder().build()
            )
            .build();

    @Test
    void give_success_find_breed() {
        when(breedRepository.findById(any())).thenReturn(Optional.of(breed));
        when(subBreedRepository.findByBreed(any())).thenReturn(subBreeds);
        when(dogService.findBreedByName(Constant.BREED_SHEEPDOG)).thenReturn(baseResponse);
    }

    @Test
    void give_success_find_breed_with_empty_data() {
        when(breedRepository.findById(any())).thenReturn(Optional.of(Breed.builder().build()));
        when(subBreedRepository.findByBreed(any())).thenReturn(List.of());
        when(dogService.findBreedByName("sheepydog")).thenReturn(responseEmpty);
    }

    @Test
    void given_save_breed_success() {
        when(breedRepository.findById(any())).thenReturn(Optional.of(breed));
        when(breedRepository.save(any())).thenReturn(breed);
        when(imageRepository.save(any())).thenReturn(image);
        when(dogService.saveBreed(any())).thenReturn(responseSuccessSave);
    }

    @Test
    void given_save_subBreed_success() {
        when(breedRepository.findById(any())).thenReturn(Optional.of(breed));
        when(breedRepository.save(any())).thenReturn(breed);
        when(imageRepository.save(any())).thenReturn(image);
        when(dogService.saveBreed(any())).thenReturn(responseSuccessSave);
    }
}
